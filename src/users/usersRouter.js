import express from 'express';

import * as usersController from './usersController';

const usersRouter = express.Router();

usersRouter.get('/', usersController.getUsers);

export default usersRouter;
