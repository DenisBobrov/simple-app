import { asyncAction } from '../utils/action';
import { getUsers as getUsersService } from './usersService';


export const getUsersDep = async (request, response, next) => {
  try {
    const users = await getUsersService();

    response.json(users);
  } catch (e) {
    next(e);
  }
};

export const getUsers = asyncAction(async (request, response) => {
  const users = await getUsersService();

  response.json(users);
});
