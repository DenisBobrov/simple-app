import axios from 'axios';
import pick from 'lodash/pick';

const jsonPlaceholderClient = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com/',
});

const userProperties = ['username', 'name', 'email'];

export const getUsers = async () => {
  const { data: users } = await jsonPlaceholderClient.get('/users');

  return users
    .map((user) => pick(user, userProperties));
};
