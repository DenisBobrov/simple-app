import express from 'express';
import bodyParser from 'body-parser';

import errorHandler from './middlewares/errorHandler';
import tokenVerifier from './middlewares/tokenVerifier';

import authRouter from './auth/authRouter';
import usersRouter from './users/usersRouter';

const PORT = process.env.PORT || 3001;

const app = express();

app.use(bodyParser());

app.use('/auth', authRouter);

app.use(tokenVerifier);

app.use('/users', usersRouter);

app.use(errorHandler);

app.listen(PORT);
