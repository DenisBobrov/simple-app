const DEFAULT_ERROR_CODE = 400;

export default (error, request, response, next) => {
  if (response.headersSent) {
    return next(error);
  }

  response.status(error.code || DEFAULT_ERROR_CODE);
  response.json({
    name: error.name,
    message: error.message,
  });
};
