import { verifyToken } from '../auth/authService';

const TOKEN_HEADER_KEY = 'x-access-token';

export default async (request, response, next) => {
  const token = request.headers[TOKEN_HEADER_KEY];

  try {
    await verifyToken(token);

    next();
  } catch (e) {
    next(e);
  }
};
