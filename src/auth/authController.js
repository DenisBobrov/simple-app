import { asyncAction } from '../utils/action';
import { loginUser } from './authService';

export const login = asyncAction(async (request, response) => {
  const { username, password } = request.body;

  const token = await loginUser({ username, password });

  response.json({
    token,
  });
});

export const logout = (request, response) => {
  response.send('logout page')
};
