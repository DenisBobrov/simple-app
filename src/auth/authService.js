import jwt from 'jsonwebtoken';
import omit from 'lodash/omit';

import { AuthenticationError } from '../utils/errors';

const SECRET_KEY = 'SJ@9djSD(@)Djw892d@(@#kdkjs0S)SD'; // todo: move to env
const sensitiveUserData = ['password'];

const trustedUsers = [
  {
    username: 'john',
    password: 'test',
  },
  {
    username: 'denis',
    password: 'test',
  },
];

export const loginUser = async ({ username, password }) => {
  const user = trustedUsers.find((trustedUser) => trustedUser.username === username && trustedUser.password === password);

  if (!user) {
    throw new AuthenticationError('User not found');
  }

  const userData = omit(user, sensitiveUserData);
  const token = jwt.sign(userData, SECRET_KEY, {
    expiresIn: '1h',
  });

  return token;
};

export const verifyToken = async (token) => {
  try {
    return await jwt.verify(token, SECRET_KEY);
  } catch (e) {
    throw new AuthenticationError('Invalid token');
  }
};

export const logoutUser = async () => {

};
