export class ClientError extends Error {
  constructor(message, code = 400) {
    super(message);

    this.name = 'ClientError';
    this.message = message;
    this.code = code;
  }
}

export class AuthenticationError extends ClientError {
  constructor(message) {
    super(message, 401);

    this.name = 'AuthenticationError';
  }
}

export class AuthorizationError extends ClientError {
  constructor(message) {
    super(message, 403);

    this.name = 'AuthenticationError';
  }
}
