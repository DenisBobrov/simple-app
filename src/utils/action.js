// export const asyncAction = (actionFn) => async (request, response, next) => {
//   try {
//     await actionFn(request, response, next);
//   } catch (e) {
//     next(e);
//   }
// };

export const asyncAction = (actionFn) => (request, response, next) =>
  Promise
    .resolve(actionFn(request, response, next))
    .catch(next);
